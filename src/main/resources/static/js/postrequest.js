$( document ).ready(function() {
    
	var url = window.location;


	function onSignIn(googleUser) {
      var profile = googleUser.getBasicProfile();
      console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
      console.log('Name: ' + profile.getName());
      console.log('Image URL: ' + profile.getImageUrl());
      console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
    }

	$("#btnUploadContract").click(function(event){
            //ajaxGet();
            document.getElementById("pnlForm").style.display = "";
    	})

	$("#btnSendContract").click(function(event){
//
            ajaxSendEnv();

    	})

    	$("#btnWorkFlow").click(function(event){

                    ajaxValidateWorkFlow();

            	})

	
    // Open Bootstrap Modal
    function openModel(){
    	$("#modalId").modal();
    }
    
    // DO GET
    function ajaxGet(){
        $.ajax({
            type : "GET",
            url : url + "/greeting",
            success: function(data){
            	// fill data to Modal Body
                fillData(data);
            },
            error : function(e) {
            	fillData(null);
            }
        }); 
    }


    function ajaxGetToken(){
            $.ajax({
                type : "GET",
                url : url + "accesstoken",
                success: function(data){
                	// fill data to Modal Body
                    fillData(data);
                },
                error : function(e) {
                	fillData(null);
                }
            });
        }

         function ajaxSendEnv(){
                    $.ajax({
                        type : "POST",
                        url : url + "/sign",
                        success: function(data){
                        	// fill data to Modal Body
                            fillData(data);
                        },
                        error : function(e) {
                        	fillData(null);
                        }
                    });
                }

                function ajaxValidateWorkFlow(){
                                    $.ajax({
                                        type : "GET",
                                        url : url + "/getContract",
                                        success: function(data){
                                        	// fill data to Modal Body
                                            fillData(data);
                                        },
                                        error : function(e) {
                                        	fillData(null);
                                        }
                                    });
                                }
    // DO GET
        function ajaxLogin(){
            $.ajax({
                type : "GET",
                url : url + "/login",
                success: function(data){
                	// fill data to Modal Body
                    fillDataLogin(data);
                },
                error : function(e) {
                	fillDataLogin(null);
                }
            });
        }
    
    function fillData(data){
    	if(data!=null){
    	    $(".container #pnlForm").html("");
            $(".container #pnlFormWorkFlow").text(data);
    	}else{
            $(".container #pnlFormWorkFlow").text("Can Not Get Data from Server!");
    	}
    }

})