package com.signature.gradstories;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.signature"})
@EntityScan("com.signature")
@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class, SecurityAutoConfiguration.class})
//@EnableAutoConfiguration(exclude = {, ManagementWebSecurityAutoConfiguration.class})
public class SpringBootTwitterBootstrapManualInstallApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootTwitterBootstrapManualInstallApplication.class, args);
	}



}

