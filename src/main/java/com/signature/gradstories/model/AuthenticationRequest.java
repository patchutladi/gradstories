package com.signature.gradstories.model;

import com.docusign.esign.api.EnvelopesApi;
import com.docusign.esign.client.ApiException;
import com.docusign.esign.model.CarbonCopy;
import com.docusign.esign.model.EnvelopeDefinition;
import com.docusign.esign.model.EnvelopeSummary;
import com.docusign.esign.model.Signer;
import com.signature.gradstories.common.WorkArguments;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class AuthenticationRequest {

    RestTemplate restTemplate = new RestTemplate();

    //send a request to get the access token
    public String getPostWithResponseHandling() {
        AuthenticationModel authenticationModel = new AuthenticationModel();
        String url = "https://account-d.docusign.com/oauth/token";
        MultiValueMap<String, Object> multiMap = new LinkedMultiValueMap<>();
        multiMap.add("grant_type", authenticationModel.getGrant_type());
        multiMap.add("assertion", authenticationModel.getAssertion());
        ResponseEntity<AuthenticationResponse> response = this.restTemplate.postForEntity(url, multiMap, AuthenticationResponse.class);
        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody().getAccess_token();
        } else {
            return null;
        }
    }







}
