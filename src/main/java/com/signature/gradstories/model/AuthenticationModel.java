package com.signature.gradstories.model;

import lombok.Data;

@Data
public class AuthenticationModel {
    private String grant_type;
    private String assertion;

    public AuthenticationModel()
    {
        //generate JWT using jwt.io
        this.setAssertion("##############TbPnEa0f8KA5Yt61251S2GERfEmOooQOcw");
        this.setGrant_type("urn:ietf:params:oauth:grant-type:jwt-bearer");
    }
}
