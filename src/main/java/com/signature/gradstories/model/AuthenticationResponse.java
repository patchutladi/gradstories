package com.signature.gradstories.model;

import lombok.Data;

@Data
public class AuthenticationResponse {

    private String access_token;
    private String  token_type;
    private String expires_in;

}
