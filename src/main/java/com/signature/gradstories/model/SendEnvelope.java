package com.signature.gradstories.model;


import com.docusign.esign.api.EnvelopesApi;
import com.docusign.esign.client.ApiException;
import com.docusign.esign.model.CarbonCopy;
import com.docusign.esign.model.EnvelopeDefinition;
import com.docusign.esign.model.EnvelopeSummary;
import com.docusign.esign.model.Signer;
import com.docusign.esign.model.Tabs;
import com.signature.gradstories.core.model.Session;
import com.signature.gradstories.core.model.User;
import org.springframework.beans.factory.annotation.Autowired;

import static com.signature.gradstories.model.AbstractEsignatureController.createEnvelopesApi;

public class  SendEnvelope  {

    @Autowired
    private Session session;

    @Autowired
    private User user;

    ///restapi/v2.1/accounts/68c89c27-0463-405e-9e0f-b877d7b8368d/chunked_uploads
    public static final String ENVELOPE_STATUS_CREATED = "created";


}
