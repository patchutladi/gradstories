package com.signature.gradstories.controller;


import com.docusign.esign.api.EnvelopesApi;
import com.docusign.esign.client.ApiException;
import com.signature.DSConfiguration;
import com.signature.gradstories.common.WorkArguments;
import com.signature.gradstories.core.model.DoneExample;
import com.signature.gradstories.core.model.Session;
import com.signature.gradstories.core.model.User;
import com.signature.gradstories.model.AbstractEsignatureController;
import com.signature.gradstories.model.AuthenticationRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;


/**
 * Get an envelope's basic information and status.<br />
 * List the basic information about an envelope, including its overall status.
 * Additional API/SDK methods may be used to get additional information about
 * the envelope, its documents, recipients, etc. This example demonstrates
 * how to obtain the latest information about an envelope from DocuSign. Often
 * an alternative is to use Connect to enable DocuSign to proactively send your
 * application updates when the status of an envelope changes.
 */
@Controller
public class ControllerEnvelopeInfo extends AbstractEsignatureController {

    private final Session session;
    private final User user;

    @Autowired
    private AuthenticationRequest authenticationRequest;

    @Autowired
    public ControllerEnvelopeInfo(DSConfiguration config, Session session, User user) {
        super(config, "eg004", "Get envelope information");
        this.session = session;
        this.user = user;
    }



    @Override
    // ***DS.snippet.0.start
    @ResponseBody
    @RequestMapping(value = "homepage/getContract", method = RequestMethod.GET)
    protected Object doWork(WorkArguments args, ModelMap model, HttpServletResponse response) throws ApiException {
        // Step 1. get envelope info
        EnvelopesApi envelopesApi = createEnvelopesApi("https://demo.docusign.net/restapi", authenticationRequest.getPostWithResponseHandling());
//        DoneExample.createDefault(title)
//                .withJsonObject(envelopesApi.getEnvelope("68c89c27-0463-405e-9e0f-b877d7b8368d", "a4eab325-9bf7-4e38-b3e4-78d03b2970e4"))
//                .addToModel(model);
        return "The Document was sent at this date time " + envelopesApi.getEnvelope("#################",
                "#################").getSentDateTime();
    }
    // ***DS.snippet.0.end
}
