package com.signature.gradstories.controller;

import com.signature.gradstories.common.WorkArguments;
import com.signature.gradstories.model.AuthenticationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

@Controller
public class WebController {

    @Autowired
    public AuthenticationRequest authentication;

    @Autowired
    public WorkArguments workArguments;

    @RequestMapping(value="/",method = RequestMethod.GET)
    public String home(){
        return "bootstraphelloworld";
    }

    @RequestMapping(value="/homepage",method = RequestMethod.GET)
    public String homepage(){
        return "index";
    }

    @ResponseBody
    @RequestMapping(value="homepage/greeting",method = RequestMethod.GET)
    public String Greeting(){
        return "Message From SpringBoot Service - Hello World!";
    }

    @ResponseBody
    @RequestMapping(value="/sign",method = RequestMethod.GET)
    public void Sign() throws IOException {
        //EG002ControllerSigningViaEmail(workArguments);
        //return "Message From SpringBoot Service - Hello World!";
    }

    @ResponseBody
    @RequestMapping(value="/accesstoken",method = RequestMethod.POST)
    public String GetToken(){
      return   authentication.getPostWithResponseHandling();
    }


}